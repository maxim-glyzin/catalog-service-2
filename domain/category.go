package domain

type Category struct {
	Articul  string `protobuf:"bytes,1,opt,name=articul,proto3" json:"articul,omitempty"`
	Name     string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Path     string `protobuf:"bytes,3,opt,name=path,proto3" json:"path,omitempty"`
	Link     string `protobuf:"bytes,4,opt,name=link,proto3" json:"link,omitempty"`
	FullLink string `protobuf:"bytes,5,opt,name=full_link,json=fullLink,proto3" json:"full_link,omitempty"`
}
