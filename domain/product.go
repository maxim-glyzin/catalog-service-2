package domain

type Product struct {
	Articul         string `protobuf:"bytes,1,opt,name=articul,proto3" json:"articul,omitempty"`
	Name            string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	StatusId        int32  `protobuf:"varint,3,opt,name=status_id,json=statusId,proto3" json:"status_id,omitempty"`
	CategoryArticul string `protobuf:"bytes,4,opt,name=category_articul,json=categoryArticul,proto3" json:"category_articul,omitempty"`
}
