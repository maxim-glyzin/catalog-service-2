module gitlab.com/maxim-glyzin/catalog-service

go 1.14

require (
	github.com/golang/protobuf v1.4.1
	github.com/jackc/pgx/v4 v4.6.0
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	google.golang.org/genproto v0.0.0-20200521103424-e9a78aa275b7 // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
)
