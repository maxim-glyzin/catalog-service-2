--DROP DATABASE "catalog-service";
--CREATE DATABASE "catalog-service";

DROP TABLE IF EXISTS "category";
CREATE TABLE "category"
(
    articul   CHAR(8) PRIMARY KEY,
    name      VARCHAR(200) NOT NULL,
    path      VARCHAR(10),
    link      VARCHAR(200),
    full_link VARCHAR(300)
);

DROP TABLE IF EXISTS "category_seo";
CREATE TABLE "category_seo"
(
    category_articul CHAR(8) PRIMARY KEY,
    trans_link       VARCHAR(200),
    meta_description varchar(500),
    meta_keywords    varchar(500),
    description      varchar(2000)
);

ALTER TABLE category_seo
    DROP CONSTRAINT IF EXISTS category_seo_fk_category_articul;
ALTER TABLE category_seo
    ADD CONSTRAINT category_seo_fk_category_articul
        FOREIGN KEY (category_articul) REFERENCES category (articul)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
        NOT VALID;

DROP TABLE IF EXISTS "product";
CREATE TABLE "product"
(
    articul          CHAR(8) PRIMARY KEY,
    name             VARCHAR(200) NOT NULL,
    category_articul CHAR(8),
    status_id        SMALLINT
);

ALTER TABLE product
    DROP CONSTRAINT IF EXISTS category_seo_fk_category_articul;
ALTER TABLE product
    ADD CONSTRAINT product_fk_articul
        FOREIGN KEY (category_articul) REFERENCES category (articul)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
        NOT VALID;


DROP TABLE IF EXISTS "product_price";
CREATE TABLE "product_price"
(
    product_articul             CHAR(8) PRIMARY KEY,
    stock_price                 FLOAT,
    small_wholesale_price       FLOAT,
    small_wholesale_delay_price FLOAT,
    wholesale_delay_price       FLOAT,
    wholesale_price             FLOAT,
    wholesale_artificial_price  FLOAT,
    sale_price                  FLOAT,
    retail_warehouse_price      FLOAT,
    special_price               FLOAT,
    special_offer_price         FLOAT
);

ALTER TABLE product_price
    DROP CONSTRAINT IF EXISTS product_price_fk_product_articul;
ALTER TABLE product_price
    ADD CONSTRAINT product_price_fk_product_articul
        FOREIGN KEY (product_articul) REFERENCES product (articul)
            ON DELETE NO ACTION
        NOT VALID;

DROP TABLE IF EXISTS product_base_attributes;
CREATE TABLE product_base_attributes
(
    product_articul   VARCHAR(8) PRIMARY KEY,
    producer_id       INT,
    country_id        INT,
    minimum_delivery  INT,
    pieces_in_pack    INT,
    pieces_in_box     INT,
    packs_in_box      INT,
    volume            FLOAT,
    weight            FLOAT,
    delivery_quant_id INT
);

ALTER TABLE product_base_attributes
    DROP CONSTRAINT IF EXISTS product_base_attributes_fk_product_articul;
ALTER TABLE product_base_attributes
    ADD CONSTRAINT product_base_attributes_fk_product_articul
        FOREIGN KEY (product_articul) REFERENCES product (articul)
            ON DELETE CASCADE
        NOT VALID;

create table country
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE product_attribute_type
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(10) NOT NULL
);

-- static values
INSERT INTO product_attribute_type (name)
VALUES ('numeric'),
       ('list'),
       ('list-many');

DROP TABLE IF EXISTS product_attribute;
CREATE TABLE product_attribute
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(40) NOT NULL CHECK (name <> '') UNIQUE,
    type_id INT         NOT NULL
);

ALTER TABLE product_attribute
    DROP CONSTRAINT IF EXISTS product_attribute_fk_type_id;
ALTER TABLE product_attribute
    ADD CONSTRAINT product_attribute_fk_type_id
        FOREIGN KEY (type_id) REFERENCES product_attribute_type (id);

ALTER SEQUENCE product_attribute_id_seq
    START WITH 110;

DROP TABLE IF EXISTS product_list_attribute_variant;
CREATE TABLE product_list_attribute_variant
(
    id           SERIAL PRIMARY KEY,
    attribute_id INT         NOT NULL,
    value        VARCHAR(50) NOT NULL check ( value <> '' )
);

ALTER TABLE product_list_attribute_variant
    DROP CONSTRAINT IF EXISTS product_list_attribute_variant_fk_attribute_id;
ALTER TABLE product_list_attribute_variant
    ADD CONSTRAINT product_list_attribute_variant_fk_attribute_id
        FOREIGN KEY (attribute_id) REFERENCES product_attribute (id)
            ON DELETE CASCADE;

/*
 Don't forget to update sequence value after migration,
 without that postgres start fill @holes@: last value 777, but 2 is omitted;
 next value will be 2 and not 778
 SELECT last_value FROM "ProductAttributes_id_seq"; -- last_value=>110
 */
ALTER SEQUENCE product_list_attribute_variant_id_seq
    START WITH 474;

/*
  --TODO: REMOVE DUPLICATES
    select v.value, count(v.value) as count
    from "ProductListAttributeVariants" v
         join "ProductAttributes" a on v.attribute_id = a.id
    group by v.value
    having count(v.value) > 1
 */
CREATE TABLE product_list_attribute_values -- "values" because list-many type
(
    id              SERIAL PRIMARY KEY,
    attribute_id    INTEGER NOT NULL,
    product_articul CHAR(8) NOT NULL,
    value_id        INT
);

ALTER TABLE product_list_attribute_values
    DROP CONSTRAINT IF EXISTS product_list_attribute_values_fk_attribute_id;
ALTER TABLE product_list_attribute_values
    ADD CONSTRAINT product_list_attribute_values_fk_attribute_id
        FOREIGN KEY (attribute_id) REFERENCES product_attribute (id)
            ON DELETE CASCADE;


ALTER TABLE product_list_attribute_values
    DROP CONSTRAINT IF EXISTS product_list_attribute_values_fk_product_articul;
ALTER TABLE product_list_attribute_values
    ADD CONSTRAINT product_list_attribute_values_fk_product_articul
        FOREIGN KEY (product_articul) REFERENCES product (articul)
            ON DELETE SET NULL
        NOT VALID;

ALTER SEQUENCE product_list_attribute_values_id_seq
    START WITH 7914;