protoc --go_out=plugins=grpc:grpc grpc/catalog.proto
# to delete omit empty from struct's tags
ls grpc/*.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
