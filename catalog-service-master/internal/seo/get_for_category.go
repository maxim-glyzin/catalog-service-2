package seo

import (
	"database/sql"
	"errors"
	"fmt"
	pb "gitlab.com/maxim-glyzin/catalog-service/grpc"
)

func GetCategorySeo(categoryPath string, db *sql.DB) (*pb.CategorySeo, error) {
	q := `
		SELECT trans_link, meta_description, meta_keywords, description
		FROM category_seo cs
		JOIN category c ON c.articul = cs.category_articul
		WHERE c.path = $1
	`
	var cs pb.CategorySeo
	row := db.QueryRow(q, categoryPath)
	switch err := row.Scan(&cs.TransLink, &cs.MetaDescription, &cs.MetaKeywords, &cs.Description); err {
	case sql.ErrNoRows:
		return nil, errors.New(fmt.Sprintf("no seo exists for path: %s\n", categoryPath))
	case nil:
		return &cs, nil
	default:
		return nil, err
	}
}
