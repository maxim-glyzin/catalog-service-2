package products

import (
	"database/sql"
	"errors"
	"fmt"
	pb "gitlab.com/maxim-glyzin/catalog-service/grpc"
	"gitlab.com/maxim-glyzin/catalog-service/pkg/sql_utils"
	"strings"
)

func GetSubcategoryProducts(categoryPath string, priceType string, db *sql.DB) ([]interface{}, []*pb.CategoryProduct, error) {
	prArticuls, err := getProductArticuls(categoryPath, db)
	if err != nil {
		return nil, nil, err
	}
	filters, err := collectFilters(prArticuls, priceType, db)
	if err != nil {
		return nil, nil, err
	}
	products, err := getProducts(prArticuls, priceType, db)
	if err != nil {
		return nil, nil, err
	}

	fmt.Println(filters)
	fmt.Println(products)

	return nil, nil, nil
}

func collectFilters(prArticuls []string, priceType string, db *sql.DB) ([]interface{}, error) {
	var filters []interface{}
	bFilters, err := collectBaseFilters(prArticuls, priceType, db)
	if err != nil {
		return nil, err
	}
	filters = append(filters, bFilters...)
	lFilters, err := collectListFilters(prArticuls, db)
	if err != nil {
		return nil, err
	}
	filters = append(filters, lFilters...)
	nFilters, err := collectNumericFilters(prArticuls, db)
	if err != nil {
		return nil, err
	}
	filters = append(filters, nFilters...)
	return filters, nil
}

// getProductArticuls gather product articuls for a given category path.
func getProductArticuls(categoryPath string, db *sql.DB) ([]string, error) {
	q := `
		SELECT articul
		FROM product
		WHERE category_articul IN (SELECT articul
                           		   FROM category
                           		   WHERE path = $1
                              		 OR path LIKE CONCAT(CAST($1 AS VARCHAR), '.%'));
	`

	rows, err := db.Query(q, categoryPath)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var productArticuls []string
	for rows.Next() {
		var a string
		err := rows.Scan(&a)
		if err != nil {
			return nil, err
		}
		productArticuls = append(productArticuls, a)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return productArticuls, nil
}

/*
	Attribute types:
	1 -> list
	2 -> numeric-list
	3 -> numeric-range
*/

type variant struct {
	id    int
	value string
}

type listFilter struct {
	attributeId        int
	attributeTypeId    int
	attributeName      string
	filteredVariantIds []int
	initialVariants    []variant
	isTouched          bool
}

type fRange struct {
	min float64
	max float64
}

type numericRangeFilter struct {
	attributeId     int
	attributeTypeId int
	attributeName   string
	filteredRange   fRange
	initialRange    fRange
	unit            string
	isTouched       bool
}
type fValue struct {
	value float64
	unit  string
}

type numericListFilter struct {
	attributeId     int
	attributeTypeId int
	attributeName   string
	filteredValues  []float64
	initialValues   []fValue
	isTouched       bool
}

func collectBaseFilters(prArticuls []string, prType string, db *sql.DB) ([]interface{}, error) {
	var baseFilters []interface{}
	// TODO: Promise.all() gathering of initial variants
	prRange, err := getPriceRange(prArticuls, prType, db)
	if err != nil {
		return nil, err
	}
	countryVariants, err := getCountryVariants(prArticuls, db)
	if err != nil {
		return nil, err
	}
	baseFilters = append(baseFilters, listFilter{
		-3,
		1,
		"статус",
		nil,
		[]variant{{id: 1, value: "new"}, {id: 3, value: "sale"}},
		false,
	})
	baseFilters = append(baseFilters, numericRangeFilter{
		-2,
		3,
		"цена",
		fRange{min: 0, max: 0},
		prRange,
		"₽",
		false,
	})
	baseFilters = append(baseFilters, listFilter{
		-1,
		2,
		"страна",
		[]int{},
		countryVariants,
		false,
	})
	return baseFilters, nil
}

func getPriceRange(prArticuls []string, prType string, db *sql.DB) (fRange, error) {
	convPrArticuls := sql_utils.ConvertToSliceOfInterfaces(prArticuls)
	q := `
		SELECT MIN(` + prType + `) AS min, MAX(` + prType + `) AS max 
		FROM product_price
		WHERE product_articul IN (` + sql_utils.CreateWhereInParams(convPrArticuls) + `);`

	row := db.QueryRow(q, convPrArticuls...)

	prRange := fRange{}
	switch err := row.Scan(&prRange.min, &prRange.max); err {
	case sql.ErrNoRows:
		return prRange, errors.New("can not get price range")
	case nil:
		return prRange, nil
	default:
		return prRange, err
	}
}

func getCountryVariants(prArticuls []string, db *sql.DB) ([]variant, error) {
	convPrArticuls := sql_utils.ConvertToSliceOfInterfaces(prArticuls)
	q := `
		SELECT id, name as value
		FROM country
		WHERE id IN (
		    SELECT DISTINCT country_id
		    FROM product_base_attributes
		    WHERE product_articul IN (` + sql_utils.CreateWhereInParams(convPrArticuls) + `)
		)
	`

	rows, err := db.Query(q, convPrArticuls...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var variants []variant
	for rows.Next() {
		var v variant
		err := rows.Scan(&v.id, &v.value)
		if err != nil {
			return nil, err
		}
		variants = append(variants, v)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return variants, nil
}

func collectListFilters(prArticuls []string, db *sql.DB) ([]interface{}, error) {
	convPrArticuls := sql_utils.ConvertToSliceOfInterfaces(prArticuls)
	q := `
		SELECT DISTINCT pa.id, pa.name, pa.type_id, var.id AS variant_id, var.value as variant_value
		FROM product_attribute pa
			JOIN product_list_attribute_values plav ON pa.id = plav.attribute_id
			JOIN product_list_attribute_variant var ON plav.value_id = var.id
		WHERE type_id in (2,3) 
			AND product_articul IN (` + sql_utils.CreateWhereInParams(convPrArticuls) + `);
	`

	rows, err := db.Query(q, convPrArticuls...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type listAttributeVariant struct {
		id           int
		name         string
		aType        int // "type" reserved keyword
		variantId    int
		variantValue string
	}

	var listAttributeVariants []listAttributeVariant
	for rows.Next() {
		lav := listAttributeVariant{}
		if err := rows.Scan(&lav.id, &lav.name, &lav.aType, &lav.variantId, &lav.variantValue); err != nil {
			return nil, err
		}
		listAttributeVariants = append(listAttributeVariants, lav)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	listFiltersMap := make(map[int]listFilter)

	for _, lav := range listAttributeVariants {
		vr := variant{id: lav.variantId, value: lav.variantValue}
		if _, ok := listFiltersMap[lav.id]; !ok {
			listFiltersMap[lav.id] = listFilter{
				attributeId:        lav.id,
				attributeTypeId:    lav.aType,
				attributeName:      lav.name,
				filteredVariantIds: []int{},
				initialVariants:    []variant{vr},
				isTouched:          false,
			}

		} else {
			lf, _ := listFiltersMap[lav.id]
			lf.initialVariants = append(lf.initialVariants, vr)
			listFiltersMap[lav.id] = lf
		}
	}

	var listFilters []interface{}
	for _, v := range listFiltersMap {
		listFilters = append(listFilters, v)
	}

	return listFilters, nil
}

func collectNumericFilters(prArticuls []string, db *sql.DB) ([]interface{}, error) {
	convPrArticuls := sql_utils.ConvertToSliceOfInterfaces(prArticuls)
	q := `
		SELECT DISTINCT pa.id,
		                pa.name,
		                pa.type_id,
		                pnav.value,
		                COALESCE(u.unit, '') AS unit
		FROM product_attribute pa
		         JOIN product_numeric_attribute_value pnav ON pa.id = pnav.attribute_id
		         LEFT JOIN product_numeric_attribute_unit u ON pnav.unit_id = u.id
		WHERE pa.type_id = 1
		  AND product_articul IN (` + sql_utils.CreateWhereInParams(convPrArticuls) + `);`

	rows, err := db.Query(q, convPrArticuls...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	type listNumericAttributeVariant struct {
		id    int
		name  string
		aType int // "type" reserved keyword
		value float64
		unit  string
	}

	var listNumericAttributeVariants []listNumericAttributeVariant
	for rows.Next() {
		lnav := listNumericAttributeVariant{}
		if err := rows.Scan(&lnav.id, &lnav.name, &lnav.aType, &lnav.value, &lnav.unit); err != nil {
			return nil, err
		}
		listNumericAttributeVariants = append(listNumericAttributeVariants, lnav)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	listNumericFiltersMap := make(map[int]numericListFilter)

	for _, lnav := range listNumericAttributeVariants {
		fV := fValue{value: lnav.value, unit: lnav.unit}
		if _, ok := listNumericFiltersMap[lnav.id]; !ok {
			listNumericFiltersMap[lnav.id] = numericListFilter{
				attributeId:     lnav.id,
				attributeTypeId: lnav.aType,
				attributeName:   lnav.name,
				filteredValues:  []float64{},
				initialValues:   []fValue{fV},
				isTouched:       false,
			}
		} else {
			lf, _ := listNumericFiltersMap[lnav.id]
			lf.initialValues = append(lf.initialValues, fV)
			listNumericFiltersMap[lnav.id] = lf
		}
	}

	var numericFilters []interface{}
	for _, v := range listNumericFiltersMap {
		numericFilters = append(numericFilters, v)
	}

	return numericFilters, nil
}

func getProducts(prArticuls []string, priceType string, db *sql.DB) ([]*pb.CategoryProduct, error) {
	convPrArticuls := sql_utils.ConvertToSliceOfInterfaces(prArticuls)
	q := `
		SELECT p.articul,
			   p.name,
			   COALESCE(p.status_id, 0)         AS status_id,
			   p.category_articul,
			   COALESCE(pr.PRICE_TYPE, 0)       AS price,           -- 0 price is unreal
		       COALESCE(pr.sale_price, 0)       AS special_price,   -- panic with conversion of null to float (real)
			   COALESCE(ba.minimum_delivery, 0) AS minimum_delivery
		FROM product p
				 LEFT JOIN product_base_attributes ba
						   ON p.articul = ba.product_articul
				 LEFT JOIN product_price pr
						   ON p.articul = pr.product_articul
		WHERE p.articul in (` + sql_utils.CreateWhereInParams(convPrArticuls) + `)
		  AND pr.small_wholesale_delay_price IS NOT NULL
		  AND pr.stock_price IS NOT NULL
	`

	rows, err := db.Query(strings.Replace(q, "PRICE_TYPE", priceType, -1), convPrArticuls...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var products []*pb.CategoryProduct
	for rows.Next() {
		cp := pb.CategoryProduct{}
		err := rows.Scan(&cp.Articul, &cp.Name, &cp.StatusId, &cp.CategoryArticul, &cp.Price, &cp.SpecialPrice, &cp.MinimumDelivery)
		if err != nil {
			return nil, err
		}
		products = append(products, &cp)
	}

	return products, nil
}
