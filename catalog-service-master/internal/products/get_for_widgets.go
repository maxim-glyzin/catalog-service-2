package products

import (
	"database/sql"
	pb "gitlab.com/maxim-glyzin/catalog-service/grpc"
	"strings"
)

type SpecialProducts struct {
	New  []*pb.CategoryProduct
	Sale []*pb.CategoryProduct
}

func GetProductsForWidgets(priceType string, db *sql.DB) (*SpecialProducts, error) {
	// use sql union two combine 2 queries (status_id=1 || status_id=3)
	q := `
		(SELECT p.articul,
			   p.value,
			   COALESCE(p.status_id, 0)         AS status_id,
			   p.category_articul,
			   COALESCE(pr.PRICE_TYPE, 0)       AS price,           -- 0 price is unreal
		       COALESCE(pr.sale_price, 0)       AS special_price,   -- panic with conversion of null to float (real)
			   COALESCE(ba.minimum_delivery, 0) AS minimum_delivery
		FROM product p
				 LEFT JOIN product_base_attributes ba
						   ON p.articul = ba.product_articul
				 LEFT JOIN product_price pr
						   ON p.articul = pr.product_articul
		WHERE p.status_id = 1
		  AND pr.small_wholesale_delay_price IS NOT NULL
		  AND pr.stock_price IS NOT NULL
		LIMIT 10)
		UNION
		(SELECT p.articul,
			   p.value,
			   COALESCE(p.status_id, 0)        	AS status_id,
			   p.category_articul,	
			   COALESCE(pr.PRICE_TYPE, 0)       AS price,
		       COALESCE(pr.sale_price, 0)       AS special_price,
			   COALESCE(ba.minimum_delivery, 0) AS minimum_delivery
		FROM product p
				 LEFT JOIN product_base_attributes ba
						   ON p.articul = ba.product_articul
				 LEFT JOIN product_price pr
						   ON p.articul = pr.product_articul
		WHERE p.status_id = 3
		  AND pr.small_wholesale_delay_price IS NOT NULL
		  AND pr.stock_price IS NOT NULL
		LIMIT 10);`
	var newProducts, saleProducts []*pb.CategoryProduct
	// TODO: is here more elegant way for replacement of @PRICE_TYPE@
	rows, err := db.Query(strings.Replace(q, "PRICE_TYPE", priceType, -1))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		cp := pb.CategoryProduct{}
		err := rows.Scan(&cp.Articul, &cp.Name, &cp.StatusId, &cp.CategoryArticul, &cp.Price, &cp.SpecialPrice, &cp.MinimumDelivery)
		if err != nil {
			return nil, err
		}
		if cp.StatusId == 1 {
			newProducts = append(newProducts, &cp)
		} else {
			saleProducts = append(saleProducts, &cp)
		}
	}

	return &SpecialProducts{New: newProducts, Sale: saleProducts}, nil
}
