package products

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// structure for object, that we get from 1c server
type product struct {
	Articul                  string  `json:"articul,omitempty"`
	Name                     string  `json:"product,omitempty"`
	CategoryArticul          string  `json:"category_articul,omitempty"`
	StatusId                 int     `json:"status_id"`
	StockPrice               float64 `json:"stock_price"`
	SmallWholesalePrice      float64 `json:"small_wholesale_price"`
	SmallWholesaleDelayPrice float64 `json:"small_wholesale_delay_price"`
	WholesaleDelayPrice      float64 `json:"wholesale_delay_price"`
	WholesalePrice           float64 `json:"wholesale_price"`
	WholesaleArtificialPrice float64 `json:"wholesale_artificial_price"`
	SalePrice                float64 `json:"sale_price"`
	RetailWarehousePrice     float64 `json:"retail_warehouse_price"`
	SpecialPrice             float64 `json:"special_price"`
	SpecialOfferPrice        float64 `json:"special_offer_price"`
	Volume                   float64 `json:"volume"`
	Weight                   float64 `json:"weight"`
}

func SyncProducts(db *sql.DB) {
	products, err := getProductsFrom1C()
	if err != nil {
		log.Printf("function -> getProductsFrom1C() failed, reason: %v,  ", err)
		return
	}

	err = insertProductsInDB(products, db)
	if err != nil { // but not shut down server
		log.Printf("function -> insertCategoriesInDB() failed, reason: %v,  ", err)
		return
	}
	log.Println("products were synchronized with 1c server")
}

/*
Example of response's body from 1c-sever
[
    {
        "articul": "00000012",
        "product": "Контейнер 500г. прямоуг. г.Пермь 108*82*42 1/100/1000",
        "category_articul": "00008458",
        "status_id": null,
        "weight": 10,
        "volume": 0.10032,
        "stock_price": 2.16,
        "small_wholesale_price": 2.38,
        "small_wholesale_delay_price": 2.48,
        "wholesale_delay_price": 2.36,
        "wholesale_price": 2.26,
        "wholesale_artificial_price": 2.3,
        "sale_price": 2.05,
        "retail_warehouse_price": 2.63,
        "special_price": 2.16,
        "special_offer_price": 2.16
    }
}
*/
func getProductsFrom1C() ([]product, error) {
	// TODO: ENV_VARS
	// create http request -> "http://188.226.58.122:8080/web1c/hs/goods/products"
	url := "http://192.168.1.1:80/web1c/hs/goods/products"
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Basic b2JtZW46Rng2NyVycG1RcXdWZz1cMjMxbm5+QWU5OUpkUF9cMzQwOVFBVGRE")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	// parse response body
	var products []product
	if err := json.NewDecoder(resp.Body).Decode(&products); err != nil {
		return nil, err
	}
	if len(products) == 0 {
		return nil, errors.New("1c server return no products")
	}
	return products, nil
}

func insertProductsInDB(products []product, db *sql.DB) error {
	// delete current products
	stmts := make([]string, 0)
	stmts = append(stmts, "ALTER TABLE product DISABLE TRIGGER ALL;")
	stmts = append(stmts, "DELETE FROM product;")
	stmts = append(stmts, "DELETE FROM product_price;")
	stmts = append(stmts, "ALTER TABLE product ENABLE TRIGGER ALL;")
	//insert new products | TODO: create general method for bulk insert
	var valPrdPlrs, valPrcPlrs, valBaPlrs []string // Prd=product; Prc=price; Ba=base_attributes; Plrs=placeholder
	var valPrdArgs, valPrcArgs, valBaArgs []interface{}
	i, j, k := 0, 0, 0
	for _, p := range products {
		// insert postgres placeholders ($1, ..., $N)
		valPrdPlrs = append(valPrdPlrs, fmt.Sprintf("($%d, $%d, $%d, $%d)", 1+i, 2+i, 3+i, 4+i))
		valPrcPlrs = append(valPrcPlrs, fmt.Sprintf("($%d, $%d, $%d, $%d, $%d, $%d, $%d, $%d, $%d, $%d, $%d)",
			1+j, 2+j, 3+j, 4+j, 5+j, 6+j, 7+j, 8+j, 9+j, 10+j, 11+j))
		valBaPlrs = append(valBaPlrs, fmt.Sprintf("($%d, $%d, $%d)", 1+k, 2+k, 3+k))
		// product data
		valPrdArgs = append(valPrdArgs, p.Articul)
		valPrdArgs = append(valPrdArgs, strings.Replace(p.Name, "\"\"", "\"", -1))
		valPrdArgs = append(valPrdArgs, p.CategoryArticul)
		valPrdArgs = append(valPrdArgs, p.StatusId)
		// price data
		valPrcArgs = append(valPrcArgs, p.Articul)
		valPrcArgs = append(valPrcArgs, p.StockPrice)
		valPrcArgs = append(valPrcArgs, p.SmallWholesalePrice)
		valPrcArgs = append(valPrcArgs, p.SmallWholesaleDelayPrice)
		valPrcArgs = append(valPrcArgs, p.WholesaleDelayPrice)
		valPrcArgs = append(valPrcArgs, p.WholesalePrice)
		valPrcArgs = append(valPrcArgs, p.WholesaleArtificialPrice)
		valPrcArgs = append(valPrcArgs, p.SalePrice)
		valPrcArgs = append(valPrcArgs, p.RetailWarehousePrice)
		valPrcArgs = append(valPrcArgs, p.SpecialPrice)
		valPrcArgs = append(valPrcArgs, p.SpecialOfferPrice)
		// base attributes data
		valBaArgs = append(valBaArgs, p.Articul)
		valBaArgs = append(valBaArgs, p.Volume)
		valBaArgs = append(valBaArgs, p.Weight)
		//update indexes
		i += 4
		j += 11
		k += 3
	}
	// prepare statements
	prdStmt := fmt.Sprintf("INSERT INTO product (articul, value, category_articul, status_id) VALUES %s",
		strings.Join(valPrdPlrs, ","))
	prcStmt := fmt.Sprintf("INSERT INTO product_price ("+
		"product_articul, "+
		"stock_price, small_wholesale_price, small_wholesale_delay_price, wholesale_delay_price, wholesale_price, "+
		"wholesale_artificial_price, sale_price, retail_warehouse_price, special_price, special_offer_price"+
		") VALUES %s", strings.Join(valPrcPlrs, ","))
	baStmt := fmt.Sprintf("INSERT INTO product_base_attributes (product_articul, volume, weight) VALUES %s "+
		"ON CONFLICT (product_articul) DO UPDATE SET volume = excluded.volume, weight = excluded.weight;",
		strings.Join(valBaPlrs, ","))
	// run all statements in one transaction
	txn, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		_ = txn.Rollback()
	}()
	// reset previous data
	for _, stmt := range stmts {
		_, err := txn.Exec(stmt)
		if err != nil {
			return err
		}
	}
	// inserts new data
	_, err = txn.Exec(prdStmt, valPrdArgs...)
	if err != nil {
		return err
	}
	_, err = txn.Exec(prcStmt, valPrcArgs...)
	if err != nil {
		return err
	}
	_, err = txn.Exec(baStmt, valBaArgs...)
	if err != nil {
		return err
	}
	// end transaction
	err = txn.Commit()
	if err != nil {
		return err
	}
	return nil
}
