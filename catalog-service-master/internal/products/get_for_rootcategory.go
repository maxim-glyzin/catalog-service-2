package products

import (
	"database/sql"
	"fmt"
	pb "gitlab.com/maxim-glyzin/catalog-service/grpc"
	"strings"
)

// get products for all descendant categories by their articuls +
// iterate throw products and throw them in desCategoriesMap +
// convert map to array of categoryWithProducts +
// return []categoryWithProducts +
func GetCategoriesWithProducts(categoryPath string, priceType string, db *sql.DB) ([]*pb.CategoryWithProducts, error) {
	desCategories, err := getDirectDescendantCategories(categoryPath, db)
	if err != nil {
		return nil, err
	}

	var paths []string
	desCategoriesMap := make(map[string]*pb.CategoryWithProducts)
	for _, c := range desCategories {
		paths = append(paths, c.Path)
		desCategoriesMap[c.Articul] = c
	}

	products, err := getProductsByCategoryArticuls(paths, priceType, db)
	if err != nil {
		return nil, err
	}

	for _, p := range products {
		// !!!you can't get memory address in the map, so we copy the whole category to append product
		if _, ok := desCategoriesMap[p.CategoryArticul]; ok { // TODO: блять, каким-то образом сюда попадают товары с левыми category_articul
			c := desCategoriesMap[p.CategoryArticul] // todo: разобраться, что это за шляпа
			c.Products = append(c.Products, p)
			desCategoriesMap[p.CategoryArticul] = c
		}
	}

	var categoriesWithProducts []*pb.CategoryWithProducts
	for _, v := range desCategoriesMap {
		categoriesWithProducts = append(categoriesWithProducts, v)
	}

	return categoriesWithProducts, nil
}

/*
Get direct descendents of given rootcategory
*/
func getDirectDescendantCategories(categoryPath string, db *sql.DB) ([]*pb.CategoryWithProducts, error) {
	q := `
		SELECT articul, value, path, full_link
		FROM category
		WHERE path ~ $1
	`
	// LIKE CONCAT($1, '.%') <-- this approach don't work, because we need only direct descendents; not all possible descendents
	// example: 1 -> 1.1, 1.2 ... and not 1.1.1 ,1.2.3 ...
	// Why do we need only direct descendents? For rendering them on the page, not for search of the products.
	var cwp []*pb.CategoryWithProducts

	rows, err := db.Query(q, "^"+categoryPath+"[.][0-9]*$")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var c pb.CategoryWithProducts
		err := rows.Scan(&c.Articul, &c.Name, &c.Path, &c.FullLink)
		if err != nil {
			return nil, err
		}
		cwp = append(cwp, &c)
	}

	return cwp, nil
}

func getProductsByCategoryArticuls(categoryPaths []string, priceType string, db *sql.DB) ([]*pb.CategoryProduct, error) {
	var products []*pb.CategoryProduct
	var query string
	for _, p := range categoryPaths {
		categoryQuery := `
		(select p.articul,
    	   p.value,
           COALESCE(p.status_id, 0)         AS status_id,
    	   p.category_articul,
    	   COALESCE(pr.PRICE_TYPE, 0)       AS price,           -- 0 price is unreal
		   COALESCE(pr.sale_price, 0)       AS special_price,   -- panic with conversion of null to float (real)
    	   COALESCE(ba.minimum_delivery, 0) AS minimum_deliver
		from (
    	     select articul
    	     from category
    	     where path = %s
    	        or path like %s
    	) as cat
    	     join product as p on p.category_articul = cat.articul
    	     join product_price as pr on pr.product_articul = p.articul
    	     left join product_base_attributes as ba on ba.product_articul = p.articul
		limit 6)
	`
		query += fmt.Sprintf(categoryQuery, "'"+p+"'", "'"+p+".%"+"'")
		query += "\nUNION"
	}
	query = query[:len(query)-len("\nUNION")]                   // delete last UNION
	query = strings.Replace(query, "PRICE_TYPE", priceType, -1) // swap priceType

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var cp pb.CategoryProduct
		err := rows.Scan(&cp.Articul, &cp.Name, &cp.StatusId, &cp.CategoryArticul, &cp.Price, &cp.SpecialPrice, &cp.MinimumDelivery)
		if err != nil {
			return nil, err
		}
		products = append(products, &cp)
	}

	return products, nil
}
