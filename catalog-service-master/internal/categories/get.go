package categories

import (
	"database/sql"
	catalog "gitlab.com/maxim-glyzin/catalog-service/grpc"
)

func Get(db *sql.DB) ([]*catalog.Category, error) {
	var categories []*catalog.Category

	rows, err := db.Query("SELECT articul, name, path, link, full_link FROM category")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		c := &catalog.Category{}
		err := rows.Scan(&c.Articul, &c.Name, &c.Path, &c.Link, &c.FullLink)
		if err != nil {
			return nil, err
		}
		categories = append(categories, c)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return categories, nil
}
