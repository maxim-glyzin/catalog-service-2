package categories

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/maxim-glyzin/catalog-service/pkg/cyr2lat"
	"log"
	"net/http"
	"strings"
)

// structure for object, that we get from 1c server
// plus two additional fields: link and full link
type category struct {
	Articul  string
	Name     string `json:"category"`
	Path     string `json:"hierarchy"`
	Link     string
	FullLink string
}

/*
Function for updating categories; delete current categories and insert new ones.
Algorithm:
1) Get response (json) from 1C
Example of response's body from 1c-sever:
[
    {
        "articul": "00003736",
        "category": "Бумажная продукция",
        "hierarchy": "1"
    }
]
2) Create hash map from postgres.category_seo
{
	"category_articul": trans_link (NOT NULL)
}
3) Fill category.Link by iterating throw resp.data
	if (trans_link) -> category.Link = trans_link
	if (!trans_link) -> category.Link = cyr2lat(category.toLowerCase())
4) Fill category.FullLink by iterating throw resp.data
5) Insert categories in db <-- TODO: rethink insertion
*/
func SyncCategories(db *sql.DB) {
	categories, err := getCategoriesFrom1C() // return err, because want exit from function,
	if err != nil {
		log.Printf("function -> getCategoriesFrom1C() failed, reason: %v\n", err)
		return
	}

	m, err := createTransLinkMap(db)
	if err != nil { // but not shut down server
		log.Printf("function -> createTransLinkMap() failed, reason: %v\n", err)
		return
	}
	fillCategoryLink(categories, &m)
	fillCategoryFullLink(categories)

	err = insertCategoriesInDB(categories, db)
	if err != nil { // but not shut down server
		log.Printf("function -> insertCategoriesInDB() failed, reason: %v\n", err)
		return
	}
	log.Println("categories were synchronized with 1c server")
}

func getCategoriesFrom1C() ([]category, error) {
	// TODO: ENV_VARS
	// create http request -> "http://188.226.58.122:8080/web1c/hs/goods/categories"
	url := "http://192.168.1.1:80/web1c/hs/goods/categories"
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Basic b2JtZW46Rng2NyVycG1RcXdWZz1cMjMxbm5+QWU5OUpkUF9cMzQwOVFBVGRE")
	resp, err := client.Do(req)
	if err != nil {
		//log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()
	// parse response body
	var categories []category
	if err := json.NewDecoder(resp.Body).Decode(&categories); err != nil {
		return nil, err
	}
	if len(categories) == 0 {
		return nil, errors.New("1c server return no categories")
	}
	return categories, nil
}

type categorySeo struct {
	CategoryArticul string
	TransLink       sql.NullString
}

func createTransLinkMap(db *sql.DB) (map[string]string, error) {
	m := map[string]string{}
	rows, err := db.Query("SELECT category_articul, trans_link FROM category_seo")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		cs := new(categorySeo)
		err := rows.Scan(&cs.CategoryArticul, &cs.TransLink)
		if err != nil {
			return nil, err
		}
		m[cs.CategoryArticul] = cs.TransLink.String
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return m, nil
}

func fillCategoryLink(categories []category, m *map[string]string) {
	for i, c := range categories {
		var link string
		if tl, ok := (*m)[c.Articul]; tl != "" && ok { // postgres.NULL === ""
			link = tl
		} else {
			link = cyr2lat.Cyr2lat(strings.ToLower(c.Name))
		}
		categories[i].Link = link
		categories[i].FullLink = ""
	}
}

func fillCategoryFullLink(categories []category) {
	for i := 0; i < len(categories); i++ {
		outer := categories[i]
		for j := i; j < len(categories); j++ {
			inner := categories[j]
			// plus is working faster, than sprintf https://tinyurl.com/y7xj9qnc
			if strings.HasPrefix(inner.Path, outer.Path+".") || inner.Path == outer.Path {
				if inner.FullLink == "" {
					categories[j].FullLink = outer.Link
				} else {
					categories[j].FullLink += "/" + outer.Link
				}
			}
		}
	}
}

func insertCategoriesInDB(categories []category, db *sql.DB) error {
	// delete current categories
	stmts := make([]string, 0)
	stmts = append(stmts, "ALTER TABLE category DISABLE TRIGGER ALL;")
	stmts = append(stmts, "DELETE FROM category;")
	stmts = append(stmts, "ALTER TABLE category ENABLE TRIGGER ALL;")
	// run preparatory statements in one transaction
	txn, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		_ = txn.Rollback()
	}()
	for _, stmt := range stmts {
		_, err := txn.Exec(stmt)
		if err != nil {
			return err
		}
	}
	err = txn.Commit()
	if err != nil {
		return err
	}
	//insert new categories
	var valStrs []string
	var valArgs []interface{}
	i := 0
	for _, c := range categories {
		valStrs = append(valStrs, fmt.Sprintf("($%d, $%d, $%d, $%d, $%d)", 1+i, 2+i, 3+i, 4+i, 5+i))
		valArgs = append(valArgs, c.Articul) // TODO: some articuls have less than 8 symbols; ask Marina where to add nulls for articul
		valArgs = append(valArgs, strings.Replace(c.Name, "\"\"", "\"", -1))
		valArgs = append(valArgs, c.Path)
		valArgs = append(valArgs, c.Link)
		valArgs = append(valArgs, c.FullLink)
		i += 5
	}
	stmt := fmt.Sprintf("INSERT INTO category (articul, name, path, link, full_link) VALUES %s", strings.Join(valStrs, ","))
	_, err = db.Exec(stmt, valArgs...)
	if err != nil {
		return err
	}
	return nil
}
