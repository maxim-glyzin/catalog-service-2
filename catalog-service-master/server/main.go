package main

import (
	"context"
	"database/sql"
	_ "github.com/jackc/pgx/v4/stdlib"
	pb "gitlab.com/maxim-glyzin/catalog-service/grpc"
	"gitlab.com/maxim-glyzin/catalog-service/internal/categories"
	"gitlab.com/maxim-glyzin/catalog-service/internal/products"
	"gitlab.com/maxim-glyzin/catalog-service/internal/seo"
	"google.golang.org/grpc"
	"log"
	"net"
)

var db *sql.DB
var err error

type catalogServer struct{}

// TODO: delete stub and calculate real price type
var priceType string = "small_wholesale_delay_price"

func main() {
	// connect to postgres
	db, err = sql.Open("pgx", "host=localhost port=5432 user=postgres password=postgres dbname=catalog-service sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	// create grpc server
	lis, err := net.Listen("tcp", "localhost:7002")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	// register our service
	pb.RegisterCatalogServiceServer(grpcServer, &catalogServer{})
	/*
		@@@@
	*/
	products.GetSubcategoryProducts("2.1", priceType, db)
	/*
		@@@@
	*/
	// run grpc server
	grpcServer.Serve(lis)
}

/*
### GRPC IMPLEMENTATIONS ###
*/

// update categories and products in postgres (data from 1c)
func (s *catalogServer) SyncCatalog(ctx context.Context, request *pb.SyncCatalogRequest) (*pb.SyncCatalogResponse, error) {
	go func() {
		categories.SyncCategories(db)
		products.SyncProducts(db)
		// TODO: use websockets, to tell frontend than we are done
	}()
	return &pb.SyncCatalogResponse{}, nil
}

// return all categories from postgres
func (s *catalogServer) GetCategories(ctx context.Context, request *pb.GetCategoriesRequest) (*pb.GetCategoriesResponse, error) {
	categories, err := categories.Get(db)
	if err != nil {
		return nil, err
	}
	return &pb.GetCategoriesResponse{Categories: categories}, nil
}

// return ten products for each widget sale/new on the index page
func (s *catalogServer) GetProductsForWidgets(ctx context.Context, request *pb.GetProductsForWidgetsRequest) (*pb.GetProductsForWidgetsResponse, error) {
	// TODO: define price type in middleware to use it here | now priceType is global variable defined upper in this file
	products, err := products.GetProductsForWidgets(priceType, db)
	if err != nil {
		return nil, err
	}
	return &pb.GetProductsForWidgetsResponse{
		NewProducts:  products.New,
		SaleProducts: products.Sale,
	}, nil
}

// return direct descendants of given root category with 6 products inside + seo
func (s *catalogServer) GetRootcategoryData(ctx context.Context, request *pb.GetRootcategoryDataRequest) (*pb.GetRootcategoryDataResponse, error) {
	// TODO: define price type in middleware to use it here | now priceType is global variable defined upper in this file
	categories, err := products.GetCategoriesWithProducts(request.CategoryPath, priceType, db)
	if err != nil {
		return nil, err
	}
	seo, err := seo.GetCategorySeo(request.CategoryPath, db)
	if err != nil {
		log.Println(err) // todo: is absence of seo critical?
	}
	return &pb.GetRootcategoryDataResponse{
		Categories: categories,
		Seo:        seo,
	}, nil
}

func (s *catalogServer) GetSubcategoryData(ctx context.Context, request *pb.GetSubcategoryDataRequest) (*pb.GetSubcategoryDataResponse, error) {
	products.GetSubcategoryProducts(request.CategoryPath, priceType, db)
	return &pb.GetSubcategoryDataResponse{
		Articul:  "",
		Name:     "",
		Path:     "",
		FullLink: "",
		Products: nil,
		Seo:      nil,
	}, nil
}
