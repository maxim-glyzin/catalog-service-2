package sql_utils

import "reflect"

// ConvertToSliceOfInterfaces create slice of interfaces for db.Query
func ConvertToSliceOfInterfaces(arg interface{}) []interface{} {
	switch reflect.TypeOf(arg).Kind() {
	case reflect.Slice:
		argSlice := reflect.ValueOf(arg)
		returnSlice := make([]interface{}, argSlice.Len())
		for i := 0; i < argSlice.Len(); i++ {
			returnSlice[i] = argSlice.Index(i).Interface()
		}
		return returnSlice
	}
	return nil
}

