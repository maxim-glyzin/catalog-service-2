package sql_utils

import "strconv"

func CreateWhereInParams(array []interface{}) string {
	var params string

	for i := 0; i < len(array); i++ {
		params += "$" + strconv.Itoa(i+1) + ","
	}

	if len(params) == 0 {
		return ""
	} else {
		return params[:len(params)-1] // delete last comma -> S1,S2,
	}
}
