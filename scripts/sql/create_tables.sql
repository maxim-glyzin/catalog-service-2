DROP TABLE IF EXISTS "category";
CREATE TABLE "category"
(
    articul   CHAR(8) PRIMARY KEY,
    name      VARCHAR(200) NOT NULL,
    path      VARCHAR(10),
    link      VARCHAR(200),
    full_link VARCHAR(300)
);

DROP TABLE IF EXISTS "product";
CREATE TABLE "product"
(
    articul          CHAR(8) PRIMARY KEY,
    name             VARCHAR(200) NOT NULL,
    category_articul CHAR(8) REFERENCES category (articul),
    status_id        SMALLINT
);
