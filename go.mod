module example.com/catalog-service

go 1.14

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/protobuf v1.4.2
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/lib/pq v1.7.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/spf13/viper v1.7.0
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
)
