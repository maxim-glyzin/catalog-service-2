package server

import (
	"database/sql"
	"example.com/catalog-service/delivery/grpc"
	"example.com/catalog-service/pkg/category/repository/postgres"
	"example.com/catalog-service/pkg/category/usecase"
	productRepo "example.com/catalog-service/pkg/product/repository/postgres"
	productUcase "example.com/catalog-service/pkg/product/usecase"
	"fmt"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

type Server struct {
	grpcServer *grpc.Server
}

// load environment variables from config file
func init() {
	if os.Getenv("ENVIRONMENT") == "development" {
		viper.SetConfigName("dev.config")
	} else {
		viper.SetConfigName("prod.config")
	}
	viper.AddConfigPath("./config")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("can't load viper config file, reason: %v", err)
	}
}

func NewServer() Server {
	// create database connection
	db, err := sql.Open("pgx", createDBConnString())
	if err != nil {
		log.Fatal(err)
	}
	// todo: решить вопрос с этой хуйней
	// defer db.Close()

	grpcServer := grpc.NewServer()

	categoryRepo := repository.NewPostgresCategoryRepository(db)
	categoryUsecase := usecase.NewCategoryUsecase(categoryRepo)

	productRepo := productRepo.NewPostgresProductRepository(db)
	productUsecase := productUcase.NewProductUsecase(productRepo)

	delivery.NewGrpcServer(grpcServer, categoryUsecase, productUsecase)

	return Server{grpcServer}
}

func (s *Server) Run() {
	// create network listener
	address := fmt.Sprintf("%s:%s", viper.GetString(`catalog.host`), viper.GetString(`catalog.port`))
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// run grpc-server
	if err := s.grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
