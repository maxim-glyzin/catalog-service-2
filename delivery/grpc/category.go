package delivery

import (
	"context"
	"example.com/catalog-service/domain"
	pb "example.com/catalog-service/protobuf"
)

// method to convert grpc category to domain category
func (s *GrpcServer) GrpcToCategory(c *pb.Category) *domain.Category {
	return &domain.Category{
		Articul:  c.Articul,
		Name:     c.Name,
		Path:     c.Path,
		Link:     c.Link,
		FullLink: c.FullLink,
	}
}

// method to convert domain category to grpc category
func (s *GrpcServer) CategoryToGrpc(c *domain.Category) *pb.Category {
	return &pb.Category{
		Articul:  c.Articul,
		Name:     c.Name,
		Path:     c.Path,
		Link:     c.Link,
		FullLink: c.FullLink,
	}
}

func (s *GrpcServer) FindCategories(ctx context.Context, req *pb.FindCategoriesRequest) (*pb.FindCategoriesResponse, error) {
	dCategories, err := s.categoryUsecase.Find()
	if err != nil {
		return nil, err
	}
	var rCategories []*pb.Category
	for _, dc := range dCategories {
		rCategories = append(rCategories, s.CategoryToGrpc(dc))
	}
	return &pb.FindCategoriesResponse{Categories: rCategories}, err
}
