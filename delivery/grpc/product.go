package delivery

import (
	"context"
	"example.com/catalog-service/domain"
	pb "example.com/catalog-service/protobuf"
)

// method to convert domain category to grpc category
func (s *GrpcServer) ProductToGrpc(c *domain.Product) *pb.Product {
	return &pb.Product{
		Articul:         c.Articul,
		Name:            c.Name,
		CategoryArticul: c.CategoryArticul,
		StatusId:        c.StatusId,
	}
}

func (s *GrpcServer) FindProducts(ctx context.Context, req *pb.FindProductsRequest) (*pb.FindProductsResponse, error) {
	dProducts, err := s.productUsecase.Find()
	if err != nil {
		return nil, err
	}
	var rProducts []*pb.Product
	for _, dp := range dProducts {
		rProducts = append(rProducts, s.ProductToGrpc(dp))
	}
	return &pb.FindProductsResponse{Products: rProducts}, err
}
