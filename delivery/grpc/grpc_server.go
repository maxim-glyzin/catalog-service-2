package delivery

import (
	categoryUcase "example.com/catalog-service/pkg/category/usecase"
	productUcase "example.com/catalog-service/pkg/product/usecase"
	pb "example.com/catalog-service/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type GrpcServer struct {
	categoryUsecase categoryUcase.CategoryUsecase
	productUsecase  productUcase.ProductUsecase
}

func NewGrpcServer(gserver *grpc.Server, categoryUsecase categoryUcase.CategoryUsecase, productUsecase productUcase.ProductUsecase) {
	pb.RegisterCatalogServiceServer(gserver, &GrpcServer{
		categoryUsecase,
		productUsecase,
	})
	reflection.Register(gserver)
}
