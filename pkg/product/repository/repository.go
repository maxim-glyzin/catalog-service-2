package repository

import (
	"example.com/catalog-service/domain"
)

type ProductRepository interface {
	Find() ([]*domain.Product, error)
}
