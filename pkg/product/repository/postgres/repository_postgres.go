package repository

import (
	"database/sql"
	"example.com/catalog-service/pkg/product/repository"
)

type productRepository struct {
	db *sql.DB
}

func NewPostgresProductRepository(db *sql.DB) repository.ProductRepository {
	return &productRepository{db}
}

