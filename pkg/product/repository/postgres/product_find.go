package repository

import "example.com/catalog-service/domain"

func (r *productRepository) Find() ([]*domain.Product, error) {
	var products []*domain.Product

	rows, err := r.db.Query("SELECT articul, name, category_articul, COALESCE(status_id, 0) AS status_id FROM product")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		p := &domain.Product{}
		err := rows.Scan(&p.Articul, &p.Name, &p.CategoryArticul, &p.StatusId)
		if err != nil {
			return nil, err
		}
		products = append(products, p)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return products, nil
}

