package usecase

import (
	"example.com/catalog-service/domain"
	"example.com/catalog-service/pkg/product/repository"
)

type ProductUsecase interface {
	Find() ([]*domain.Product, error)
}

type productUsecase struct {
	repo repository.ProductRepository
}

func NewProductUsecase(repo repository.ProductRepository) *productUsecase {
	return &productUsecase{repo}
}
