package usecase

import "example.com/catalog-service/domain"

func (u *productUsecase) Find() ([]*domain.Product, error) {
	products, err := u.repo.Find()
	if err != nil {
		return nil, err
	}
	return products, nil
}
