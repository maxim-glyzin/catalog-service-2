package usecase

import "example.com/catalog-service/domain"

func (u *categoryUsecase) Find() ([]*domain.Category, error) {
	categories, err := u.repo.Find()
	if err != nil {
		return nil, err
	}
	return categories, nil
}
