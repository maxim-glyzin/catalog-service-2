package usecase

import (
	"example.com/catalog-service/domain"
	"example.com/catalog-service/pkg/category/repository"
)

type CategoryUsecase interface {
	Find() ([]*domain.Category, error)
}

type categoryUsecase struct {
	repo repository.CategoryRepository
}

func NewCategoryUsecase(repo repository.CategoryRepository) *categoryUsecase {
	return &categoryUsecase{repo}
}
