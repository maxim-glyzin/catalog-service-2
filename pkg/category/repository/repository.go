package repository

import (
	"example.com/catalog-service/domain"
)

type CategoryRepository interface {
	Find() ([]*domain.Category, error)
}
