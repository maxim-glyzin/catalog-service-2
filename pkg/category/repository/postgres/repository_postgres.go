package repository

import (
	"database/sql"
	"example.com/catalog-service/pkg/category/repository"
)

type categoryRepository struct {
	db *sql.DB
}

func NewPostgresCategoryRepository(db *sql.DB) repository.CategoryRepository {
	return &categoryRepository{db}
}

