package repository

import "example.com/catalog-service/domain"

func (r *categoryRepository) Find() ([]*domain.Category, error) {
	var categories []*domain.Category

	rows, err := r.db.Query("SELECT articul, name, path, link, full_link FROM category")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		c := &domain.Category{}
		err := rows.Scan(&c.Articul, &c.Name, &c.Path, &c.Link, &c.FullLink)
		if err != nil {
			return nil, err
		}
		categories = append(categories, c)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return categories, nil
}

